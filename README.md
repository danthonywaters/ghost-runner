# Ghost Runner
**Contributor**: De'Cayla Anthony-Waters

**Description**: Ghost Runner is a side scroller that features a ghost named Morgan as the main character. There are five difficulty levels, and your objective is to avoid the enemy ghosts for as long as possible. 

## How to Play
You must install the Processing sound library for this game to work. Go to Sketch -> Import Library -> Add Library, and then search for and install the "Sound" library by The Processing Foundation.

You start in the welcome screen. Use the up and down arrows to change the difficulty, and click anywhere on the screen to start. Use the left mouse button to jump, and avoid the oncoming green ghosts. Survive for as long as you can!
