import processing.core.PApplet;
import processing.sound.*;

//class for creating Ghosts. used in the game
class Ghost extends PApplet {
  
    public String linkToImage;
  
    //constructor - runs when an object is created
    public Ghost(String _linkToImage) {
        linkToImage = _linkToImage;
    }
    
    //returns the image from the link
    public PImage getImage(){
        return loadImage(linkToImage);
    }
  
}

//class for the actual game
public class GhostRunner extends PApplet{

  //creating Ghost objects
  Ghost mainGhost = new Ghost("https://project.mifw.co/mainGhostFinal.png");
  Ghost enemyGhost = new Ghost("https://project.mifw.co/enemyfinal.png");
 
  
  
  PImage backpic, mainghost, enemyghost, welcome;
  //x and y variables are the coordinates of the main ghost
  int game, score, highscore, x, y, vertical, difficulty;
  int enemyx[] = new int[2];
  int enemyy[] = new int[2];
  
  void settings() {
     //set sizes of window and fonts
    size(600, 800);
   
  }
  
  void setup() {
    
      //background music
      SoundFile soundtrack = new SoundFile(this, "https://project.mifw.co/soundtrack.wav");
      soundtrack.amp(0.01);
      soundtrack.play();
      
      //pull the images 
      backpic = loadImage("https://project.mifw.co/backgroundFinal.jpg");
      mainghost = mainGhost.getImage();
      enemyghost = enemyGhost.getImage();
      welcome = loadImage("https://project.mifw.co/welcome_9.png");
      
      //set default settings
      //game=1 is the welcome screen, game=0 is the actual gameplay
      game = 1;
      score = 0;
      highscore = 0;
      difficulty = 1;
      
      //changes color of high score, sets font size
      textSize(20);
      
  }
  
  //draw method runs every frame
  void draw() {
    
    boolean gameIsRunning = (game == 0);
    
    if (gameIsRunning) {
      
      //this moves the background
      imageMode(CORNER);
      image(backpic, x, 0);
      image(backpic, x + backpic.width, 0);
      
      //sets speed of background
      x -= 5;
      
      //makes ghost fall from top to bottom
      vertical += 1;
      y += vertical;
      
      //restarts from the beginning 
      if (x == -600) x = 0;
      
      //handles enemy creation and collision
      for (int i = 0; i < 1; i++) {
        
        //creates the ghost
        imageMode(CENTER);
        image(enemyghost, enemyx[i], enemyy[i] - (enemyghost.height / 2 + 0));
        
        //if enemy ghost goes off the screen, reset it's position to the right
        if (enemyx[i] < 0) {
          enemyy[i] = (int) random(200, height - 200);
          enemyx[i] = width;
        }
        
        //if you pass the ghost, increase your score
        if (enemyx[i] == width / 2) highscore = max(++score, highscore);
        
        boolean characterIsOffScreen = y > height || y < 0;
        
        //if our x and y coordinates are too close to the enemy (within 30 px for x, and 60 for y)
        boolean xValuesTooClose = abs(width / 2 - enemyx[i]) < 30;
        boolean yValuesTooClose = abs(y - enemyy[i]) < 60;
        boolean characterCollidesWithEnemy = xValuesTooClose && yValuesTooClose;
        
        //if you collide with the ghost, stop and go back to welcome screen
        if (characterIsOffScreen || characterCollidesWithEnemy) game = 1;
        
        //sets the speed of the enemy ghosts based on difficulty
        if (difficulty == 1) {
           enemyx[i] -= 6 * (difficulty);
        } else {
          enemyx[i] -= 6 * (difficulty / 2);
        }
        
      }
      
      //creates the ghost you play as
      image(mainghost, width / 2, y);
      //creates/updates the score counter
      text("Score: " + score, 10, 20);
      
    } else {
      
      //handles the welcome screen, displays high score
      imageMode(CENTER);
      image(welcome, width / 2, height / 2);
      fill(167,89,170);
      text("High Score: " + highscore, 50, 650);
      fill(192,146,5);
      text("Difficulty: " + difficulty, 50, 700);
      text("Use the up and down arrow keys to set the difficulty", 50, 675);
      
    }
    
  }
  
  void mousePressed() {
    
    boolean onWelcomeScreen = (game == 1);
    
    //makes the ghost jump up
    vertical = -15;
    
    if (onWelcomeScreen) {
      //if it's the welcome screen, restart the ghost's position, then start the game
      enemyx[0] = 600;
      enemyy[0] = y = height / 2;
      enemyx[1] = 900;
      enemyy[1] = 600;
      x = game = score = 0;
    }
  }
  
  void keyPressed(){
    
    boolean onWelcomeScreen = (game == 1);
    if (onWelcomeScreen) {
       if (key == CODED) {
          if (keyCode == UP && difficulty < 5) {
            difficulty += 1;
          } else if (keyCode == DOWN && difficulty > 1) {
            difficulty -= 1; 
          }
       }
    }
  }
  
  //this launches the game when you press play
  public static void main(String[] args) {
     String[] processingArgs = {"GhostRunner"};
     GhostRunner game = new GhostRunner();
     PApplet.runSketch(processingArgs, game);
  }

}
